.PHONY:
init: down volume up
down:
	docker-compose down
volume:
	docker volume prune -f
pull:
	docker-compose pull
build:
	docker-compose build
up: pull build
	docker-compose up -d
	make ps
ps:
	docker-compose ps
test:
	docker-compose run --rm crehana_publica python -m pytest tests/ -v -s
prune:
	make down
	docker volume prune -f
	docker system prune -f
lint:
	docker-compose run --rm crehana_publica poetry run pre-commit run --all-files
